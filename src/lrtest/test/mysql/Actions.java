package lrtest.test.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import lrapi.*;

public class Actions
{
	static String url = "jdbc:mysql://localhost:3306/atp?characterEncoding=UTF-8";
	static Connection conn = null;
	static Statement stm = null;
	
	static String select_data_sql = "select * from at_user limit 100;";
	static String insert_data_sql = "";
	
	
    public int init() throws Throwable {
    	Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection(url);
		stm = conn.createStatement();
        return 0;
    }//end of init
 
    
    public int action() throws Throwable {
    	lr.start_transaction("mysql_select");
    	stm.executeUpdate(select_data_sql);	
    	lr.end_transaction("mysql_select", 0);
        return 0;
    }//end of action
 
    
    public int end() throws Throwable {
    	if (stm != null) {
			stm.close();
		}
		if (conn != null) {
			conn.close();
		}
        return 0;
    }//end of end
}